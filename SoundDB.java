import javax.sound.sampled.*;
public class SoundDB {
	private boolean soundSupported = true;
	private boolean soundEnabled = true;
	private static final String[] filenames = {"shoot.wav", "hit.wav", "explode.wav"};
	private Clip[] sounds;
	public SoundDB() {
		sounds = new Clip[filenames.length];
	}
	private Clip loadSound(String name) {
		Clip c = null;
		try {
			Line.Info i = new Line.Info(Clip.class);
			c = (Clip) AudioSystem.getLine(i);
			c.open(AudioSystem.getAudioInputStream(getClass().getResource(name)));
		}
		catch (UnsupportedAudioFileException e) {
			System.err.println("Audio file format is not supported");
			soundSupported = false;
		}
		catch (java.io.IOException e) {
			System.err.println("Audio file not found");
			soundSupported = false;
		}
		catch (LineUnavailableException e) {
			System.err.println("Unable to load sound because a system resource was unavailable");
		}
		return c;
	}
	public void init() {
		for (int x = 0; x < filenames.length; x++) {
			sounds[x] = loadSound("data/sound/" + filenames[x]);
		}
	}
	public void playSound(int index) {
		if(soundSupported && soundEnabled) {
			//sounds[index].stop();
			if(!sounds[index].isActive()) {
				sounds[index].setFramePosition(0);
				sounds[index].start();
			}
		}
	}
	public void setSoundEnabled(boolean e) {
		soundEnabled = e;
	}
	public boolean getSoundEnabled() {
		return soundEnabled;
	}
}
