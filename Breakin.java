// Breakin.java
// Copyright 2011 Daniel Johnson. All rights reserved.
//
// This program is open source software. You may use and redistribute it under
// a 2-clause (Simplified) BSD License. A copy of this license should have been
// included with this source file, see the file LICENSE. If it has not been
// provided to you, please contact the distributor of this software.


import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Area;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;

public class Breakin extends JPanel {
	private static final long serialVersionUID = -1756726447621037619L;
	private BufferedImage backbuffer; //Holds the screen image before it is painted onscreen
	public ArrayList<Pellet> pellets;//ArrayList holding all the pellets
	public ArrayList<block> blocks; //arraylist holding all the blocks
	public String levelName;
	int score;
	int maxScore;
	public Player mainplayer;
	private static JFrame window;//the JFrame displaying the Game
	private static JFileChooser jfchooser;
	private Input keyinput;    //a class that manages all the keyboard input in the game.
	javax.swing.Timer gametimer = new javax.swing.Timer(15,new Listener());//the timer that runs the game.@approx 65 FPS
	static final int GAMESTATE_RUNNING = 1;
	static final int GAMESTATE_STOPPED = 0;
	static final int GAMESTATE_MAINMENU = 2;
	static final int GAMESTATE_PAUSED = 3;
	static final int GAMESTATE_EDITOR = 4;
	int gamestate = GAMESTATE_RUNNING;
	boolean hasNextLevel = false;
	boolean testMode = false;
	int selectedBlock = -1; //selected block in editor
	boolean editorMoveMode = false;
	boolean lastWon; //If the most recently played game was won
	public static final int WIDTH = 1024;    //the width of the playing field
	public static final int HEIGHT = 768;  //the height of the playing field
	int PLAYERSPEED = 1;  //How far a player moves with each update. value loaded in NewGame()
	int PELLETDIAMETER;  //The Diameter of pellets. value loaded in NewGame()
	int MAX_LIVES; //The number of lives a player gets. loaded in loadLevel
	final int TABLERADIUS = 350;
	final int PADDLEWIDTH = 50;
	final int PADDLEHEIGHT = 20;
	final Color PURPLE = new Color((float).5,0,(float).5);
	final Color TABLECOLOR = Color.BLUE.darker();

	MainMenu mm;
	LevelDB ldb = new LevelDB();
	SoundDB sdb = new SoundDB();

	public static void main(String[] args) {
		JFrame tempwin = new JFrame();
		tempwin.setTitle("Initializing");
		tempwin.setSize(0,0);
		//tempwin.setResizeable(false);
		tempwin.setVisible(true);
		Insets in = tempwin.getInsets();
		tempwin.setVisible(false);
		tempwin.dispose();
		window = new JFrame();
		window.setLocation(0,0);
		window.setTitle("BreakIn");
		window.setBackground(Color.BLACK);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		if (screenSize.width == 1024) {
			window.setUndecorated(true);
			window.setSize(WIDTH,HEIGHT);
		}
		else if (screenSize.width < 1024) {
			javax.swing.JOptionPane.showMessageDialog(null, "Sorry, but BreakIn does not support screen resolutions lower than 1024x768.");
			return;
		}
		else {
			window.setSize(WIDTH+in.left+in.right,HEIGHT+in.top+in.bottom);
			//window.setResizeable(false);
		}


		if (screenSize.width == 1024) {
			window.setLocation(0,0);
		}
		else {
			window.setLocationRelativeTo(null);
		}
		window.setContentPane(new Breakin());
		window.setVisible(true);
		jfchooser = new JFileChooser();
		//jfchooser.setDialogType(JFileChooser.SAVE_DIALOG);
		jfchooser.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("TXT Files", "txt"));
	}
	public Breakin() {
		this.setSize(WIDTH,HEIGHT);
		backbuffer = new BufferedImage (WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
		backbuffer.createGraphics();
		addKeyListener(new KeyListener());
		setFocusable(true);
		keyinput = new Input();
		//NewGame();
		ldb.init();
		sdb.init();
		mm = new MainMenu();
		showMenu();
	}
	void saveLevel() {
		if (jfchooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			saveLevel(jfchooser.getSelectedFile());
		}
		requestFocus();
	}
	void saveLevel(String filename) {
		saveLevel(new File(filename));
	}
	void saveLevel(File file) {
		try {
			PrintWriter filestream = new PrintWriter(new BufferedWriter(new FileWriter(file)),true);
			filestream.println("Version=2");

			filestream.println("\n[Info]");
			filestream.println("Lives = " + MAX_LIVES);
			filestream.println("Name = " + levelName);

			filestream.println("\n[Blocks]");
			for (block b : blocks) {
				filestream.println(b.getCenterX()+","+b.getCenterY()+","+b.getType());
			}
			filestream.close();
		}
		catch (IOException ex) {
			System.out.println("I/O Error, aborting save");
		}
	}
	void loadLevel() {
		if (jfchooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			loadLevel(jfchooser.getSelectedFile().toString());
		}
		requestFocus();
	}
	void loadLevel(String dir, int number) {
		loadLevel(new InputStreamReader(getClass().getResourceAsStream("levels/" + dir + "/level" + number + ".txt")));

	}
	void loadLevel(String filename) {
		try {
			loadLevel(new FileReader(new File(filename)));
		}
		catch (FileNotFoundException ex) {
			System.out.println("File Not Found!: "+filename);
		}
	}
	void trimData(String[] data) {
		for (int i = 0; i < data.length; i++) {
			data[i] = data[i].trim();
		}
	}
	void loadLevel(InputStreamReader is) {
		BufferedReader filestream;
		try {
			filestream = new BufferedReader(is);
			int version = 0;
			final int supported_version = 2;
			int section = 0; //1 = Info, 2 = Blocks

			double centerx;
			double centery;
			int type;
			String[] data;
			String line = "";
			blocks = new ArrayList<block>();
			line = filestream.readLine().trim();
			data = line.split("=");
			trimData(data);
			if (data.length == 2 && data[0].equalsIgnoreCase("version")) {
				version = Integer.parseInt(data[1]);
			}
			else {
				System.out.println("Cannot load this file, it is either too old or corrupt");
				return;
			}
			if (version > supported_version) {
				System.out.println("Cannot load this file; it is using a newer save file format than this version of Breakin supports.");
				return;
			}

			line = filestream.readLine();
			while (line !=null) {
				line = line.trim();
				if (!line.equals("")) {
					if (line.charAt(0) == '[') {
						line = line.substring(1, line.length()-1);
						if (line.equalsIgnoreCase("Info")) {
							section = 1;
						}
						else if (line.equalsIgnoreCase("Blocks")) {
							section = 2;
						}
						else {
							section = 0;
						}
					}
					else if (section == 1) {
						data = line.split("=");
						trimData(data);
						if (data[0].equalsIgnoreCase("lives")) {
							MAX_LIVES = Integer.parseInt(data[1]);

						}
						else if (data[0].equalsIgnoreCase("name"))
							levelName = data[1];
					}
					else if (section == 2) {
						data = line.split(",");
						trimData(data);
						centerx = Double.parseDouble(data[0]);
						centery = Double.parseDouble(data[1]);
						type = Integer.parseInt(data[2]);
						blocks.add(new block(centerx,centery,type));
					}
				}
				line = filestream.readLine();
			}
			is.close();
		}
		catch (IOException ex) {
			System.out.println("I/O Error while loading level");
		}
	}
	public static double normalizeAngle(double theta) {
		return (theta + Math.PI * 2.0) % (Math.PI * 2.0);
	}
	public static double angleDiff(double A, double B) {
		double diff = normalizeAngle(A - B);
		if (diff > Math.PI) {
			diff -= Math.PI * 2.0;
		} else if ( diff < - Math.PI) {
			diff += Math.PI * 2.0;
		}
		return diff;
	}
	public static double getDistanceBetween2Points(double x1,double y1,double x2,double y2) {
		double xdiff = x2-x1;
		double ydiff = y2-y1;
		return (Math.sqrt(xdiff * xdiff + ydiff * ydiff));
	}
	public static double getDistanceBetween(Player play, Pellet pel) {
		return getDistanceBetween2Points(play.getCenterX(),play.getCenterY(),pel.getCenterX(),pel.getCenterY());
	}
	public static Color getLightColor() {
		return new Color((float).5,(float).5,(float)1);
	}
	class LevelDB {
		public Image[][] previews;
		public String[][] levelNames;
		public ArrayList<String> levelSetIDs;
		public ArrayList<String> levelSetNames;
		public ArrayList<Integer> levelSetCounts;
		void init() {
			loadLevelSetINI();
			previews = new Image[levelSetIDs.size()][];
			levelNames = new String[levelSetIDs.size()][];
			for (int k =0;k<previews.length;k++) {
				previews[k] = new Image[levelSetCounts.get(k)];
				levelNames[k] = new String[levelSetCounts.get(k)];
				for (int j = 0; j < previews[k].length; j++) {
					BufferedImage bf = new BufferedImage (WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
					loadLevel(levelSetIDs.get(k), j+1);
					drawBoard(bf.createGraphics());
					previews[k][j] = bf;
					levelNames[k][j] = levelName;
				}
			}
		}
		public String getLevelName(int levelset, int num) {
			return levelNames[levelset][num];
		}
		public Image getLevelPreview(int levelset, int num) {
			return previews[levelset][num];
		}
		public int getNumLevelSets() {
			return levelSetIDs.size();
		}
		public String getLevelSetName(int index) {
			return levelSetNames.get(index);
		}
		public int getLevelSetCount(int index) {
			return levelSetCounts.get(index);
		}
		private void loadLevelSetINI() {
			loadLevelSetINI(new InputStreamReader(getClass().getResourceAsStream("levels/levelsets.ini")));
		}
		private void loadLevelSetINI(InputStreamReader is) {
			BufferedReader filestream;
			try {
				filestream = new BufferedReader(is);
				int section = -1;

				levelSetIDs = new ArrayList<String>();
				levelSetNames = new ArrayList<String>();
				levelSetCounts = new ArrayList<Integer>();

				String[] data;
				String line;

				line = filestream.readLine();
				while (line !=null) {
					line = line.trim();
					if (!line.equals("")) {
						if (line.charAt(0) == '[') {
							line = line.substring(1, line.length()-1);
							levelSetIDs.add(line);
							levelSetNames.add("");
							levelSetCounts.add(0);
							section ++;
						}
						else {
							data = line.split("=");
							trimData(data);
							if (data[0].equalsIgnoreCase("name")) {
								levelSetNames.set(section, data[1]);
							}
							else if (data[0].equalsIgnoreCase("count")) {
								levelSetCounts.set(section, Integer.parseInt(data[1]));
							}
						}
					}
					line = filestream.readLine();
				}
				is.close();
			}
			catch (IOException ex) {
				System.out.println("I/O Error while loading levelset INI file");
			}
		}
		void loadLevelFromSet(int levelset, int level) {
			loadLevel(levelSetIDs.get(levelset), level);
		}
	}
	class MainMenu {
		BufferedImage menubuffer = new BufferedImage (WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
		Image helpscreen;
		Graphics menusurface = menubuffer.createGraphics();
		int selected=0;
		int levelsel=0;
		int levelsetsel = 0;
		double anim=0;
		int animd=1;
		int levelpixelscroll=0;
		int levelsetpixelscroll=0;
		//int PlayerSizeSel=25;
		int PelletSizeSel=10;
		final int ListSize=5;
		final int spacebetweenlines = 25;
		final int xoffset = 30;
		final int yoffset = 20;
		final int tabspace = 85;
		boolean helpshowing = false;
		MainMenu() {
			helpscreen = new ImageIcon(getClass().getClassLoader().getResource("help.png")).getImage();
			menusurface.setFont(new Font("SansSerif",Font.BOLD,15));
			DrawB();
		}
		void Show() {
			//NewGameCurrentSettings();
			gamestate = GAMESTATE_MAINMENU;
		}
		void update() {
			int levelpixelscrolltarget = (levelsel - 1) * 300;
			if (levelpixelscrolltarget < 0)
				levelpixelscrolltarget = 0;
			if (levelpixelscrolltarget > 300 * (ldb.getLevelSetCount(levelsetsel)- 3))
				levelpixelscrolltarget = 300 * (ldb.getLevelSetCount(levelsetsel) - 3);
			int diff = levelpixelscrolltarget - levelpixelscroll;
			int vel = diff / 20;
			levelpixelscroll += vel;

			int levelsetpixelscrolltarget = (levelsetsel - 1) * 300;
			if (levelsetpixelscrolltarget < 0)
				levelsetpixelscrolltarget = 0;
			if (levelsetpixelscrolltarget > 300 * (ldb.getNumLevelSets() - 3))
				levelsetpixelscrolltarget = 300 * (ldb.getNumLevelSets() - 3);
			diff = levelsetpixelscrolltarget - levelsetpixelscroll;
			vel = diff / 20;
			levelsetpixelscroll += vel;
		}
		void Draw(Graphics g) {
			if (helpshowing==true) {
				g.drawImage(helpscreen,0,0,null);
			}
			else {
				DrawB(false);
				g.drawImage(menubuffer,0,0,null);
			}
		}
		void DrawB() {
			DrawB(true);
		}
		void DrawB(boolean notreally) {
			anim+=.5*animd;
			if (anim>=5 || anim <=-5)
				animd*=-1;
			if (notreally) return;
			int counter = 0;
			menusurface.setColor(Color.black);
			menusurface.fillRect(0,0,WIDTH,HEIGHT);
			while (counter != ListSize) {
				setSelectedColor(counter,selected);
				if (counter == 0) {
					double myanim=0;
					//if (selected==counter)
					//	myanim=anim;


					menusurface.drawString("Level Set Selection:",0+xoffset,counter*spacebetweenlines+yoffset);
					for (int curLevelSet = 0; curLevelSet < ldb.getNumLevelSets(); curLevelSet++) {
						setSelectedColor(levelsetsel, curLevelSet);
						menusurface.fillRect(90 + 300 * curLevelSet - levelsetpixelscroll, 2*spacebetweenlines+yoffset, 220, 170);
						menusurface.drawImage(ldb.getLevelPreview(curLevelSet,0), 100 + 300 * curLevelSet - levelsetpixelscroll, 2*spacebetweenlines+yoffset+10, 200, 150, null);
						menusurface.drawString(ldb.getLevelSetName(curLevelSet), 100 + 300 * curLevelSet - levelsetpixelscroll, 3*spacebetweenlines+yoffset+170);
					}
				}
				else if (counter == 1) {
					menusurface.drawString("Level Selection:",0+xoffset,4*spacebetweenlines+yoffset + 170);
					for (int curLevel = 0; curLevel < ldb.getLevelSetCount(levelsetsel); curLevel++) {
						setSelectedColor(levelsel, curLevel);
						menusurface.fillRect(90 + 300 * curLevel - levelpixelscroll, 5 * spacebetweenlines + yoffset + 170, 220, 170);
						menusurface.drawImage(ldb.getLevelPreview(levelsetsel, curLevel), 100 + 300 * curLevel - levelpixelscroll, 5*spacebetweenlines+yoffset+180, 200, 150, null);
						menusurface.drawString(ldb.getLevelName(levelsetsel, curLevel), 100 + 300 * curLevel - levelpixelscroll, 6*spacebetweenlines+yoffset+170*2);
					}

				}
				/*else if (counter == 2){
				  menusurface.drawString("Pellet Size:",0+xoffset,counter*spacebetweenlines+yoffset);
				  setSelectedColor(PelletSizeSel,3);
				  menusurface.drawString("Tiny",tabspace+xoffset,counter*spacebetweenlines+yoffset);
				  setSelectedColor(PelletSizeSel,5);
				  menusurface.drawString("Small",tabspace*2+xoffset,counter*spacebetweenlines+yoffset);
				  setSelectedColor(PelletSizeSel,10);
				  menusurface.drawString("Large",tabspace*3+xoffset,counter*spacebetweenlines+yoffset);
				  }*/

				else if (counter == 2) {
					menusurface.drawString("Editor",0+xoffset,HEIGHT-yoffset-2*spacebetweenlines);//counter*spacebetweenlines+yoffset);
				}
				else if (counter == 3) {
					menusurface.drawString("Help",0+xoffset,HEIGHT-yoffset-1*spacebetweenlines);//counter*spacebetweenlines+yoffset);
				}
				else if (counter == 4) {
					menusurface.drawString("Exit",0+xoffset,HEIGHT-yoffset);//counter*spacebetweenlines+yoffset);
				}
				counter++;
			}
			//drawPlayer((Graphics2D)menusurface,15,selected*spacebetweenlines+yoffset-7,PADDLEWIDTH,PADDLEHEIGHT,0);
		}
		void setSelectedColor(int var1,int con1) {
			if (var1==con1) {
				menusurface.setColor(Color.yellow);
			}
			else {
				menusurface.setColor(Color.white);
			}
		}
		void moveUp() {
			selected--;
			if (selected < 0) {
				selected=ListSize-1;
			}
			DrawB();
		}
		void moveDown() {
			selected++;
			if (selected >= ListSize) {
				selected=0;
			}
			DrawB();
		}
		void nextLevel() {
			levelsel++;
			NewGameCurrentSettings();
		}
		void NewGameCurrentSettings() {
			if(!testMode)
				hasNextLevel = levelsel < ldb.getLevelSetCount(levelsetsel) - 1;
			NewGame(levelsetsel, levelsel,PLAYERSPEED,PelletSizeSel);
		}
		void cycleForward() {
			if (helpshowing) return;
			if (selected == 0) {
				levelsetsel++;
				if (levelsetsel>=ldb.getNumLevelSets())
					levelsetsel=0;
				levelsel = 0;
				DrawB();
			}
			else if (selected == 1) {
				levelsel++;
				if (levelsel>=ldb.getLevelSetCount(levelsetsel))
					levelsel=0;
				DrawB();
			}
		}
		void cycleBackward() {
			if (helpshowing) return;
			if (selected == 0) {
				levelsetsel--;
				if (levelsetsel < 0)
					levelsetsel=ldb.getNumLevelSets()-1;
				levelsel = 0;
				DrawB();
			}
			else if (selected == 1) {
				levelsel--;
				if (levelsel < 0)
					levelsel=ldb.getLevelSetCount(levelsetsel)-1;
				DrawB();
			}
		}
		void Enter() {
			if (helpshowing==true) {
				helpshowing=false;
			}
			else {
				if (selected==0) {
					selected++;
				}
				else if (selected==1) {
					testMode = false;
					score = 0;
					maxScore = 0;
					NewGameCurrentSettings();
				}
				else if (selected == 2) {
					ldb.loadLevelFromSet(levelsetsel, levelsel + 1);
					edit();
				}
				else if (selected == 3) {
					helpshowing=true;
				}
				else if (selected == 4) {
					gametimer.stop();
					window.setVisible(false);
					//window.close();
					window.dispose();
					window=null;
					System.exit(0);
				}
			}
		}
	}
	void NewGame(int levelset, int level,int playerspeed,int pelletdiameter) {
		PLAYERSPEED=playerspeed;
		PELLETDIAMETER=pelletdiameter;
		pellets = new ArrayList<Pellet>();

		if (testMode) {
			for (block b : blocks) {
				b.reset();
			}
		}
		else {
			//loadLevel(level+1);
			ldb.loadLevelFromSet(levelset, level + 1);
		}
		maxScore += getBlocksRemaining();
		mainplayer=new Player(0);
		keyinput=new Input();
		gamestate = GAMESTATE_RUNNING;
		gametimer.start();
	}
	void showMenu() {

		gametimer.start();
		mm.Show();
	}
	void NewGame() {
		score = 0;
		maxScore = 0;
		mm.NewGameCurrentSettings();
	}
	void GameOver() {
		lastWon = false;
		gamestate = GAMESTATE_STOPPED;
	}
	void Win() {
		lastWon = true;
		gamestate = GAMESTATE_STOPPED;
	}
	void Pause() {
		gamestate = GAMESTATE_PAUSED;
	}
	void UnPause() {
		gamestate = GAMESTATE_RUNNING;
	}
	void edit() {
		selectedBlock = -1;
		editorMoveMode = false;
		for (block b : blocks) {
			b.reset();
		}
		if (blocks.size() > 0) {
			selectedBlock = 0;
			blocks.get(0).setSelected(true);
		}
		gamestate = GAMESTATE_EDITOR;
	}
	void test() {
		hasNextLevel = false;
		testMode = true;
		NewGame();
	}
	int getBlocksRemaining() {
		int count = 0;
		for (block b : blocks) {
			if (b.getType()==1 && b.exists) {
				count++;
			}
		}
		return count;
	}
	void checkForWin() {
		if (getBlocksRemaining() == 0) {
			Win();
		}

	}
	boolean update() {
		keyinput.update();
		if (gamestate == GAMESTATE_RUNNING) {
			mainplayer.check4Collisions();
			for (ListIterator<Pellet> it = pellets.listIterator(); it.hasNext();) {
				Pellet p = it.next();
				p.move();
				if (p.isDead()) {
					it.remove();
					if (pellets.size()<1) {
						mainplayer.die();
					}
				}
			}
		}
		else if (gamestate == GAMESTATE_MAINMENU) {
			mm.update();
		}

		return true;
	}
	void drawLevelTitleText(Graphics2D draw) {
		Font f = new Font("SansSerif",Font.BOLD,20);
		TextLayout tl = new TextLayout(levelName, f, draw.getFontRenderContext());
		draw.setColor(Color.WHITE);
		tl.draw(draw, (float)(WIDTH/2 - tl.getBounds().getWidth() / 2.0), (float)(tl.getBounds().getHeight() + 5));
	}
	void drawEditorHelp(Graphics2D draw) {
		String s = "Arrow Keys: Move" + "\n" +
		"Space bar: Toggle move/select" + "\n" +
		"/: Change type" + "\n" +
		"DEL: Delete block" + "\n" +
		"A: Add block";
		draw.setFont(new Font("SansSerif",Font.BOLD,20));
		draw.setColor(Color.WHITE);
		String[] parts = s.split("\n");
		for (int i = 0; i < parts.length; i++) {
			draw.drawString(parts[i], 15, 25*(i+1));
		}
		s = "S: Save" + "\n" +
		"L: Load" + "\n" +
		"T: Test level" + "\n" +
		"N: New level" + "\n" +
		"M: Change level name" + "\n" +
		"ESC: Exit level editor";
		parts = s.split("\n");
		for (int i = 0; i < parts.length; i++) {
			draw.drawString(parts[i], 15, HEIGHT - (parts.length-i) *25 - 5);
		}
	}
	void drawBoard(Graphics2D draw) {
		draw.setColor(Color.black);
		draw.fillRect(0,0,WIDTH,HEIGHT);

		draw.setColor(TABLECOLOR);
		draw.fillOval(-TABLERADIUS + (WIDTH/2),-TABLERADIUS+(HEIGHT/2),2*TABLERADIUS,2*TABLERADIUS);
		for (block b : blocks) {
			b.draw(draw);
		}
		if (gamestate == GAMESTATE_EDITOR && selectedBlock != -1) {
			blocks.get(selectedBlock).draw(draw);
		}
	}
	int getStringWidth(String msg, Font f) {
		java.awt.geom.Rectangle2D msgR2D = f.getStringBounds(msg, new java.awt.font.FontRenderContext(new java.awt.geom.AffineTransform(), false, false));
		java.awt.geom.Rectangle2D.Double msgR2DD = new java.awt.geom.Rectangle2D.Double();
		msgR2DD.setRect(msgR2D);
		return (int)msgR2DD.getWidth();
	}
	void drawOverlayText(Graphics2D draw, String msg, int pos, int numLines) {
		Font f = new Font("SansSerif",Font.BOLD,30);
		TextLayout tl = new TextLayout(msg, f, draw.getFontRenderContext());
		AffineTransform posT = new AffineTransform();
		posT.setToTranslation((int)(WIDTH/2 - tl.getBounds().getWidth() / 2.0), (int)(HEIGHT/2 - (tl.getBounds().getHeight() + 5) * (numLines - 2) / 2.0 + (tl.getBounds().getHeight() + 5) * pos));
		draw.setColor(Color.BLACK);
		draw.setStroke(new BasicStroke(5.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		draw.draw(tl.getOutline(posT));
		draw.setColor(Color.WHITE);
		draw.fill(tl.getOutline(posT));
	}
	void display(Graphics2D draw) {
		if (gamestate != GAMESTATE_MAINMENU)
			drawBoard(draw);

		if (gamestate == GAMESTATE_EDITOR) {
			drawLevelTitleText(draw);
			drawEditorHelp(draw);
		}
		if ((gamestate == GAMESTATE_STOPPED)||(gamestate == GAMESTATE_RUNNING)||(gamestate == GAMESTATE_PAUSED)) {
			drawLevelTitleText(draw);
			mainplayer.draw(draw);

			for (Pellet p : pellets) {
				p.draw(draw);
			}

			Font oldfont=draw.getFont();
			draw.setFont(new Font("SansSerif",Font.BOLD,30));
			draw.setColor(Color.BLUE);
			String msg = "Lives: " + mainplayer.getLives();
			int msgWidth = getStringWidth(msg, draw.getFont());
			draw.drawString(msg, WIDTH - msgWidth - 15,35);
			msg = "Score: " + score + "/" + maxScore;
			draw.drawString(msg, 15, 35);
			draw.drawString("Blocks Remaining: " + getBlocksRemaining(),WIDTH - 360, HEIGHT-5);
			draw.setFont(oldfont);
			if (gamestate == GAMESTATE_PAUSED) {
				drawOverlayText(draw, "Paused", 0, 3);
				drawOverlayText(draw, "Esc-Menu, F2-Restart, Any other key-Continue", 2, 3);
			}
			if (gamestate == GAMESTATE_STOPPED) {
				if(lastWon) {
					if(hasNextLevel) {
						drawOverlayText(draw, "You Win!", 0, 4);
						drawOverlayText(draw, "Press Enter to Continue...", 1, 4);
						drawOverlayText(draw, "Esc to go back, F2 to restart.", 3, 4);
					}
					else {
						drawOverlayText(draw, "You Win!", 0, 4);
						drawOverlayText(draw, "Final score for levels played: " + score + "/" + maxScore, 1, 4);
						drawOverlayText(draw, "Press Esc to go back, F2 to restart.", 3, 4);
					}
				}
				else {
					drawOverlayText(draw, "Game Over", 0, 3);
					drawOverlayText(draw, "Press Esc to go back, F2 to restart.", 2, 3);
				}
			}
		}
		if (gamestate == GAMESTATE_MAINMENU) {
			mm.Draw(draw);
		}
		repaint();
		draw.dispose();
	}
	void drawPlayer(Graphics2D g2,double X, double Y, int LEN, int WID, double theta, double tilt) {
		Shape shp = getPaddleShape(X, Y, LEN, WID, theta, tilt);
		g2.setColor(PURPLE);
		g2.fill(shp);
	}
	static Shape getPaddleShape(double X, double Y, int LEN, int WID, double angle, double tilt) {
		int curx = (int)(X + WIDTH/2);
		int cury = (int)(Y+ HEIGHT/2);
		angle += tilt;
		Polygon shp = new Polygon();
		curx += Math.cos(angle-Math.PI/2.0)*(LEN/2);
		cury += Math.sin(angle-Math.PI/2.0)*(LEN/2);
		shp.addPoint(curx,cury);
		curx += Math.cos(angle)*(WID);
		cury += Math.sin(angle)*(WID);
		shp.addPoint(curx,cury);
		curx += Math.cos(angle+Math.PI/2.0)*(LEN);
		cury += Math.sin(angle+Math.PI/2.0)*(LEN);
		shp.addPoint(curx,cury);
		curx += Math.cos(angle+Math.PI)*(WID);
		cury += Math.sin(angle+Math.PI)*(WID);
		shp.addPoint(curx,cury);
		return shp;
	}
	class Pellet {
		private double xpos;
		private double ypos;
		private double theta; //in radians
		private double speed = 2;
		private double oldxpos,oldypos;
		private boolean dead = false;
		Pellet(double x, double y, double angle) {
			xpos=x;
			ypos=y;
			oldxpos=xpos;
			oldypos=ypos;
			theta = angle;
		}
		double getX() {
			return xpos - (PELLETDIAMETER/2);
		}
		double getY() {
			return ypos - (PELLETDIAMETER/2);
		}
		double getCenterX() {
			return xpos;
		}
		double getCenterY() {
			return ypos;
		}
		double getAngle() {
			return theta;
		}
		double getSpeed() {
			return speed;
		}
		boolean isDead() {
			return dead;
		}
		public void setAngle(double nangle) {
			theta = nangle;
		}
		public void setSpeed(double spd) {
			speed=spd;
		}
		public void move() {
			double oldx = xpos;
			double oldy = ypos;
			xpos = xpos + speed * Math.cos(theta);
			boolean bounced = checkHCollisions();

			ypos = ypos + speed * Math.sin(theta);
			if (!bounced) {
				bounced = checkVCollisions();
			}
			if (bounced) {
				xpos=oldx;
				ypos=oldy;
			}
			if (getDistanceBetween2Points(0,0,getCenterX(),getCenterY())>TABLERADIUS) {
				destroy();
			}
		}
		void destroy() {
			dead=true;
		}
		private boolean checkHCollisions() {
			boolean bounce = false;
			block b;

			for (ListIterator<block> it = blocks.listIterator(); it.hasNext();) {
				b = it.next();
				if (b.exists && b.shape.intersects(getX()+WIDTH/2, getY()+HEIGHT/2, PELLETDIAMETER, PELLETDIAMETER)) {
					bounce = true;
					theta = Math.atan2(Math.sin(theta), -Math.cos(theta));
					b.destroy();
					if (b.getType()==2) {
						destroy();
					}
					break;
				}
			}
			checkForWin();
			return bounce;
		}
		private boolean checkVCollisions() {
			boolean bounce = false;
			block b;
			for (ListIterator<block> it = blocks.listIterator(); it.hasNext();) {
				b = it.next();
				if (b.exists && b.shape.intersects(getX()+WIDTH/2, getY()+HEIGHT/2, PELLETDIAMETER, PELLETDIAMETER)) {
					bounce = true;
					theta = Math.atan2(-Math.sin(theta), Math.cos(theta));
					b.destroy();
					if (b.getType()==2) {
						destroy();
					}
					break;
				}
			}
			checkForWin();
			return bounce;
		}
		void draw(Graphics g) {
			g.setColor(Color.WHITE);

			g.fillOval((int)(xpos - (PELLETDIAMETER/2) +(WIDTH/2)),(int)(ypos - (PELLETDIAMETER/2) + (HEIGHT/2)),PELLETDIAMETER,PELLETDIAMETER);
			oldxpos=xpos;
			oldypos=ypos;
		}

	}
	class Player {
		private double MAX_TILT = Math.PI / 18;//10 degrees
		private double theta = 0;
		private double tilt = 0;
		private int lives = MAX_LIVES;
		private int accel=0;
		private Shape paddleshape;
		private boolean haspellet = true;
		Player(int startangle) {
			theta = startangle;
		}
		public void draw(Graphics g) {
			drawPlayer((Graphics2D)g, getCenterX(), getCenterY(), 50, 20, theta, tilt);
			if (haspellet) {
				double pelletx = Math.cos(theta)*(TABLERADIUS-25 - PADDLEHEIGHT/2);
				double pellety = Math.sin(theta)*(TABLERADIUS-25 - PADDLEHEIGHT/2);
				new Pellet(pelletx, pellety, theta + Math.PI).draw(g);
			}
		}
		void move(int dir, int tiltdir) {
			if (gamestate == GAMESTATE_RUNNING) {
				accel+=dir;
				if (dir==0) {
					if (accel>0)
						accel-=1;
					else if (accel<0)
						accel+=1;
				}
				if (accel<-6)
					accel=-6;
				if (accel>6)
					accel=6;
				theta += PLAYERSPEED*accel/5.0 * Math.PI / 180.0; //Use degrees for speed calculations
				if (theta >= Math.PI * 2.0) {
					theta -= Math.PI * 2.0;
				}
				else if (theta < 0) {
					theta += Math.PI * 2.0;
				}

				if (tiltdir == 0) {
					double sign = Math.signum(tilt);
					tilt -= Math.PI / 180.0 * sign;
					if (Math.signum(tilt) * sign < 0) {//If tilt has changed sign
						tilt = 0;
					}
				}
				else if (Math.abs(tilt) < MAX_TILT) {
					tilt += Math.PI / 180.0 * tiltdir;//1 degree increments
				}
			}
		}

		void fire() {
			if (haspellet) {
				double pelletx = Math.cos(theta)*(TABLERADIUS-25 - PADDLEHEIGHT/2);
				double pellety = Math.sin(theta)*(TABLERADIUS-25 - PADDLEHEIGHT/2);
				pellets.add(new Pellet(pelletx, pellety, theta + tilt + Math.PI));
				sdb.playSound(0);
				haspellet=false;
			}
		}
		double getCenterX() {
			return getCenterX(theta);
		}
		double getCenterY() {
			return getCenterY(theta);
		}
		private double getCenterX(double ptheta) {
			return Math.cos(ptheta)*(TABLERADIUS-25);
		}
		private double getCenterY(double ptheta) {
			return Math.sin(ptheta)*(TABLERADIUS-25);
		}
		double getAngle() {
			return theta;
		}
		void check4Collisions() {
			paddleshape=getPaddleShape(getCenterX(),getCenterY(),50,20, theta, tilt);
			Area pa = new Area(paddleshape);

			for (Pellet p : pellets) {
				if  (pa.intersects(p.getX()+(WIDTH/2), p.getY()+(HEIGHT/2), PELLETDIAMETER, PELLETDIAMETER)) {
					double diff = angleDiff(theta + tilt, p.getAngle());
					if (diff < Math.PI/2)
						p.setAngle(p.getAngle() + Math.PI + diff * 2);
				}
			}

		}
		void die() {
			score--;
			if (lives <= 1) {
				GameOver();
			}
			else {
				lives--;
				haspellet = true;
			}
		}
		int getLives() {
			return lives;
		}

	}
	class block {
		public Polygon shape;
		private double xpos;
		private double ypos;
		public boolean exists;
		private int dyingcount=0;
		private int startType = 0;
		private int type = 0;//0-normal, 1-target, 2-failiftouch, 3-unbreakable, 4-double-hit, 5-split, 6-explode
		private final int NUM_TYPES = 7;
		private boolean selected = false;
		block(double centerx,double centery,int blocktype) {
			xpos = centerx;
			ypos = centery;
			startType = type = blocktype;
			exists=true;
			shape = new Polygon();
			shape.addPoint((int)(xpos - 25 + WIDTH/2), (int)(ypos - 25 + HEIGHT/2));
			shape.addPoint((int)(xpos + 25 + WIDTH/2), (int)(ypos - 25 + HEIGHT/2));
			shape.addPoint((int)(xpos + 25 + WIDTH/2), (int)(ypos + 25 + HEIGHT/2));
			shape.addPoint((int)(xpos - 25 + WIDTH/2), (int)(ypos + 25 + HEIGHT/2));
		}
		void reset() {
			type = startType;
			exists = true;
			dyingcount = 0;
			selected = false;
		}
		double getCenterY() {
			return ypos;
		}
		double getCenterX() {
			return xpos;
		}
		int getType() {
			return type;
		}
		void changeType() {
			type++;
			if (type >= NUM_TYPES)
				type = 0;
			startType = type;
		}
		void setCenterY(double y) {
			ypos = y;
			shape = new Polygon();
			shape.addPoint((int)(xpos - 25 + WIDTH/2), (int)(ypos - 25 + HEIGHT/2));
			shape.addPoint((int)(xpos + 25 + WIDTH/2), (int)(ypos - 25 + HEIGHT/2));
			shape.addPoint((int)(xpos + 25 + WIDTH/2), (int)(ypos + 25 + HEIGHT/2));
			shape.addPoint((int)(xpos - 25 + WIDTH/2), (int)(ypos + 25 + HEIGHT/2));
		}
		void setCenterX(double x) {
			xpos = x;
			shape = new Polygon();
			shape.addPoint((int)(xpos - 25 + WIDTH/2), (int)(ypos - 25 + HEIGHT/2));
			shape.addPoint((int)(xpos + 25 + WIDTH/2), (int)(ypos - 25 + HEIGHT/2));
			shape.addPoint((int)(xpos + 25 + WIDTH/2), (int)(ypos + 25 + HEIGHT/2));
			shape.addPoint((int)(xpos - 25 + WIDTH/2), (int)(ypos + 25 + HEIGHT/2));
		}
		void setSelected(boolean s) {
			selected = s;
		}
		void draw(Graphics2D g2) {
			if (selected && gamestate == GAMESTATE_EDITOR) {
				if (editorMoveMode)
					g2.setColor(Color.RED);
				else
					g2.setColor(Color.WHITE);
				g2.fillRect((int)(xpos-25-1*dyingcount+WIDTH/2.0) - 4,(int)(ypos-25-1*dyingcount+HEIGHT/2.0) - 4,50+2*dyingcount + 8,50+2*dyingcount + 8);
				g2.setColor(Color.BLACK);
				g2.fillRect((int)(xpos-25-1*dyingcount+WIDTH/2.0) - 2,(int)(ypos-25-1*dyingcount+HEIGHT/2.0) - 2,50+2*dyingcount + 4,50+2*dyingcount + 4);

			}
			if (exists) {
				if (type == 1) {
					g2.setColor(Color.GREEN);
				}
				else if (type == 2) {
					g2.setColor(Color.RED);
				}
				else if (type == 3) {
					//g2.setColor(Color.GRAY);
					g2.setColor(new Color(0.7f,0.7f,0.7f));//GRAY
				}
				else if (type == 4) {
					g2.setColor(Color.YELLOW.darker().darker());
				}
				else if (type == 5) {
					g2.setColor(Color.WHITE);
				}
				else if (type == 6) {
					g2.setColor(new Color(255,80,0));
				}
				else {
					g2.setColor(Color.YELLOW);
				}
				g2.fillPolygon(shape);
			}
			else {
				if (dyingcount<10) {
					if (type == 1) {
						g2.setColor(new Color(0.0f,1.0f,0.0f,(10-dyingcount)/10.0f));//GREEN
					}
					else if (type == 2) {
						g2.setColor(new Color(1.0f,0.0f,0.0f,(10-dyingcount)/10.0f));//RED
					}
					else if (type == 3) {
						g2.setColor(new Color(0.7f,0.7f,0.7f,(10-dyingcount)/10.0f));//GRAY
					}
					else if (type == 4) {
						g2.setColor((new Color(1.0f,1.0f,0.0f,(10-dyingcount)/10.0f)).darker().darker());//YELLOW
					}
					else if (type == 5) {
						g2.setColor(new Color(1.0f,1.0f,1.0f,(10-dyingcount)/10.0f));//WHITE
					}
					else {
						g2.setColor(new Color(1.0f,1.0f,0.0f,(10-dyingcount)/10.0f));//YELLOW
					}

					if (type == 6) {
						g2.setColor(Color.ORANGE);
						g2.fillOval((int)(xpos - 3 * 25 + (2 * 25 / 10.0 * dyingcount) + WIDTH/2.0),
						            (int)(ypos - 3 * 25 + (2 * 25 / 10.0 * dyingcount) + HEIGHT/2.0),
							    (int)(2*(3 * 25 - (2 * 25 / 10.0 * dyingcount))),
							    (int)(2*(3 * 25 - (2 * 25 / 10.0 * dyingcount))));
					} else {
						g2.fillRect((int)(xpos-25-1*dyingcount+WIDTH/2.0),(int)(ypos-25-1*dyingcount+HEIGHT/2.0),50+2*dyingcount,50+2*dyingcount);
					}
				}
				else if (dyingcount == 10) {
					if (type == 5) {
						pellets.add(new Pellet(getCenterX(), getCenterY(), Math.random() * Math.PI * 2.0));
					}
					else if (type == 6) {
						for (block b : blocks) {
							if (Math.sqrt( Math.pow(getCenterX() - b.getCenterX(),2) + Math.pow(getCenterY() - b.getCenterY(),2))  < 25 * 3)
								b.destroy();
						}
					}
				}
				dyingcount++;
			}
		}
		void undraw(Graphics2D g2) {
			g2.setColor(TABLECOLOR);
			g2.fillPolygon(shape);
		}
		void destroy() {
			if (type == 6) {
				exists = false;
				sdb.playSound(2);
			}
			else if (type == 4) {
				type = 0;
				exists = true;
				sdb.playSound(1);
			}
			else if (type == 3) {
				exists = true;
			}
			else if (type == 1) {
				if (exists) {
					exists=false;
					sdb.playSound(1);
					score++;
				}
			}
			else {
				exists=false;
				sdb.playSound(1);
			}
		}
	}
	class Input {
		private boolean[] Pressed = new boolean[600];
		private final int MENUUP = 38; //UpArrow
		private final int MENUDOWN = 40;//DownArrow
		private final int MENULEFT = 37;//LeftArrow
		private final int MENURIGHT = 39;//RightArrow
		private final int P1LEFT = 37;//LeftArrow
		private final int P1RIGHT = 39;//RightArrow
		private final int P1TILTLEFT = KeyEvent.VK_Z;
		private final int P1TILTRIGHT = KeyEvent.VK_X;
		private final int FIRE = KeyEvent.VK_SPACE;
		private final int ESC = 27;
		private final int MUTE = KeyEvent.VK_M;
		private final int KEYF2 = 113;
		private final int ENTER = 10;
		private final int NEXT_LEVEL = KeyEvent.VK_ENTER;
		private final int EDITOR_NEXT_BLOCK = KeyEvent.VK_PERIOD;
		private final int EDITOR_PREV_BLOCK = KeyEvent.VK_COMMA;
		private final int EDITOR_MOVE_LEFT = KeyEvent.VK_LEFT;
		private final int EDITOR_MOVE_RIGHT = KeyEvent.VK_RIGHT;
		private final int EDITOR_MOVE_UP = KeyEvent.VK_UP;
		private final int EDITOR_MOVE_DOWN = KeyEvent.VK_DOWN;
		private final int EDITOR_CHANGE_TYPE = KeyEvent.VK_SLASH;
		private final int EDITOR_SAVE = KeyEvent.VK_S;
		private final int EDITOR_LOAD = KeyEvent.VK_L;
		private final int EDITOR_DELETE_BLOCK = KeyEvent.VK_DELETE;
		private final int EDITOR_ADD_BLOCK = KeyEvent.VK_A;
		private final int EDITOR_TEST = KeyEvent.VK_T;
		private final int EDITOR_NEW = KeyEvent.VK_N;
		private final int EDITOR_TOGGLE_MOVE = KeyEvent.VK_SPACE;
		private final int EDITOR_CHANGE_NAME= KeyEvent.VK_M;
		private int delay_count = 0;


		void keyPressed(int key) {
			if (gamestate == GAMESTATE_PAUSED) {
				if (key == ESC) {
					if (!Pressed[key]) {
						if (testMode)
							edit();
						else
							showMenu();
					}
				}
				else if (key == KEYF2) {
					NewGame();
				}
				else {
					UnPause();
				}
			}
			else if (gamestate == GAMESTATE_RUNNING) {
				if (!Pressed[key]) {
					if (key == ESC) {
						Pause();
					}
					else if (key == FIRE) {
						mainplayer.fire();
					}
					else if (key == MUTE) {
						sdb.setSoundEnabled(!sdb.getSoundEnabled());
					}
					/*else if (key == P1SHOOT){
					  mainplayer.shoot();
					  }*/

				}
			}
			else if (gamestate == GAMESTATE_STOPPED) {
				if (!Pressed[key]) {
					if (key == ESC) {
						if (testMode)
							edit();
						else
							showMenu();
					}
					else if (key == KEYF2) {
						NewGame();
					}
					else if (key == NEXT_LEVEL) {
						if(hasNextLevel && lastWon) {
							mm.nextLevel();
						}
					}
				}
			}
			else if (gamestate == GAMESTATE_MAINMENU) {
				if (!Pressed[key]) {
					if (key == MENUUP) {
						mm.moveUp();
					}
					if (key == MENUDOWN) {
						mm.moveDown();
					}
					if (key == MENURIGHT) {
						mm.cycleForward();
					}
					if (key == MENULEFT) {
						mm.cycleBackward();
					}
					if (key == ENTER) {
						mm.Enter();
					}
				}
			}
			else if (gamestate == GAMESTATE_EDITOR) {
				if (!Pressed[key]) {
					if (key == ESC) {
						showMenu();
					}
					if (key == EDITOR_NEXT_BLOCK) {
						if (blocks.size() > 0) {
							if (selectedBlock != -1)
								blocks.get(selectedBlock).setSelected(false);
							selectedBlock++;
							if (selectedBlock >= blocks.size())
								selectedBlock=0;
							blocks.get(selectedBlock).setSelected(true);
						}
					}
					if (key == EDITOR_PREV_BLOCK) {
						if (selectedBlock != -1)
							blocks.get(selectedBlock).setSelected(false);
						selectedBlock--;
						if (selectedBlock <= -1)
							selectedBlock = blocks.size() - 1;
						if (selectedBlock != -1)
							blocks.get(selectedBlock).setSelected(true);
					}
					if (key == EDITOR_MOVE_LEFT) {
						if (selectedBlock != -1) {
							if (editorMoveMode)
								blocks.get(selectedBlock).setCenterX(blocks.get(selectedBlock).getCenterX() - 10);
							else {
								block closest = null;
								double closestDist = -1;
								int closestpos = -1;
								for (int count = blocks.size() - 1; count >= 0; count--) {//go backwards so that == case works
									block b = blocks.get(count);
									if (Math.abs(blocks.get(selectedBlock).getCenterY() - b.getCenterY()) < 35) {
										if (b.getCenterX() < blocks.get(selectedBlock).getCenterX()) {
											double newDist = blocks.get(selectedBlock).getCenterX() - b.getCenterX();
											if (closest == null || newDist < closestDist) {
												closest = b;
												closestDist = newDist;
												closestpos = count;
											}
										}
										else if (b.getCenterX() == blocks.get(selectedBlock).getCenterX()) {
											if (count < selectedBlock) {
												closest = b;
												closestDist = 0.0;
												closestpos = count;
											}
										}
									}
								}
								if (closest != null) {
									blocks.get(selectedBlock).setSelected(false);
									selectedBlock = closestpos;
									blocks.get(selectedBlock).setSelected(true);
								}
							}
						}
					}
					if (key == EDITOR_MOVE_RIGHT) {
						if (selectedBlock != -1) {
							if (editorMoveMode)
								blocks.get(selectedBlock).setCenterX(blocks.get(selectedBlock).getCenterX() + 10);
							else {
								block closest = null;
								double closestDist = -1;
								int closestpos = -1;
								for (int count = 0; count < blocks.size(); count++) {
									block b = blocks.get(count);
									if (Math.abs(blocks.get(selectedBlock).getCenterY() - b.getCenterY()) < 35) {
										if (b.getCenterX() > blocks.get(selectedBlock).getCenterX()) {
											double newDist = -blocks.get(selectedBlock).getCenterX() + b.getCenterX();
											if (closest == null || newDist < closestDist) {
												closest = b;
												closestDist = newDist;
												closestpos = count;
											}
										}
										else if (b.getCenterX() == blocks.get(selectedBlock).getCenterX()) {
											if (count > selectedBlock) {
												closest = b;
												closestDist = 0.0;
												closestpos = count;
											}
										}
									}
								}
								if (closest != null) {
									blocks.get(selectedBlock).setSelected(false);
									selectedBlock = closestpos;
									blocks.get(selectedBlock).setSelected(true);
								}
							}
						}
					}
					if (key == EDITOR_MOVE_UP) {
						if (selectedBlock != -1) {
							if (editorMoveMode)
								blocks.get(selectedBlock).setCenterY(blocks.get(selectedBlock).getCenterY() - 10);
							else {
								block closest = null;
								double closestDist = -1;
								int closestpos = -1;
								int count;
								double newDist;
								block b;
								for (count = 0; count < blocks.size(); count++) {
									b = blocks.get(count);
									if (b.getCenterY() + 35 < blocks.get(selectedBlock).getCenterY()) {
										newDist = blocks.get(selectedBlock).getCenterY() - b.getCenterY();
										if (closest == null || newDist < closestDist) {
											closest = b;
											closestDist = newDist;
											closestpos = count;
										}
									}
								}
								if (closest != null) {
									double targety = closest.getCenterY();
									double targetx = blocks.get(selectedBlock).getCenterX();
									closestDist = Math.abs(targetx - closest.getCenterX());
									for (count = 0; count < blocks.size(); count++) {
										b = blocks.get(count);
										if (Math.abs(targety - b.getCenterY()) < 35) {
											newDist = Math.abs(targetx - b.getCenterX());
											if (newDist < closestDist) {
												closest = b;
												closestDist = newDist;
												closestpos = count;
											}
										}
									}

									blocks.get(selectedBlock).setSelected(false);
									selectedBlock = closestpos;
									blocks.get(selectedBlock).setSelected(true);
								}
							}
						}
					}
					if (key == EDITOR_MOVE_DOWN) {
						if (selectedBlock != -1) {
							if (editorMoveMode)
								blocks.get(selectedBlock).setCenterY(blocks.get(selectedBlock).getCenterY() + 10);
							else {
								block closest = null;
								double closestDist = -1;
								int closestpos = -1;
								int count;
								double newDist;
								block b;
								for (count = 0; count < blocks.size(); count++) {
									b = blocks.get(count);
									if (b.getCenterY() - 35 > blocks.get(selectedBlock).getCenterY()) {
										newDist = -blocks.get(selectedBlock).getCenterY() + b.getCenterY();
										if (closest == null || newDist < closestDist) {
											closest = b;
											closestDist = newDist;
											closestpos = count;
										}
									}
								}
								if (closest != null) {
									double targety = closest.getCenterY();
									double targetx = blocks.get(selectedBlock).getCenterX();
									closestDist = Math.abs(targetx - closest.getCenterX());
									for (count = 0; count < blocks.size(); count++) {
										b = blocks.get(count);
										if (Math.abs(targety - b.getCenterY()) < 35) {
											newDist = Math.abs(targetx - b.getCenterX());
											if (newDist < closestDist) {
												closest = b;
												closestDist = newDist;
												closestpos = count;
											}
										}
									}

									blocks.get(selectedBlock).setSelected(false);
									selectedBlock = closestpos;
									blocks.get(selectedBlock).setSelected(true);
								}
							}
						}
					}
					if (key == EDITOR_TOGGLE_MOVE) {
						editorMoveMode = !editorMoveMode;
					}
					if (key == EDITOR_CHANGE_TYPE) {
						if (selectedBlock != -1) {
							blocks.get(selectedBlock).changeType();
						}
					}
					if (key == EDITOR_CHANGE_NAME) {
						String rv = JOptionPane.showInputDialog(window.getContentPane(), "Level name:", levelName);
						if (!rv.equals("")) {
							levelName = rv.trim();
						}
						requestFocus();
					}
					if (key == EDITOR_SAVE) {
						saveLevel();
					}
					if (key == EDITOR_LOAD) {
						editorMoveMode = false;
						loadLevel();
					}
					if (key == EDITOR_DELETE_BLOCK) {
						editorMoveMode = false;
						if (selectedBlock != -1) {
							blocks.remove(selectedBlock);
							selectedBlock --;
							if (selectedBlock < 0)
								selectedBlock = blocks.size() - 1;
							blocks.get(selectedBlock).setSelected(true);
						}
					}
					if (key == EDITOR_ADD_BLOCK) {
						editorMoveMode = true;
						if (selectedBlock != -1)
							blocks.get(selectedBlock).setSelected(false);
						blocks.add(new block(0, 0, 0));
						selectedBlock = blocks.size() - 1;
						blocks.get(selectedBlock).setSelected(true);
					}
					if (key == EDITOR_TEST) {
						test();
					}
					if (key == EDITOR_NEW) {
						editorMoveMode = false;
						levelName = "Custom";
						selectedBlock = -1;
						blocks.clear();
						MAX_LIVES=5;
					}
				}
			}
			Pressed[key]=true;
		}
		void keyReleased(int key) {
			Pressed[key]=false;
			if (key == EDITOR_MOVE_LEFT || key == EDITOR_MOVE_RIGHT || key == EDITOR_MOVE_UP || key == EDITOR_MOVE_DOWN) {
				delay_count = 0;
			}
		}
		void update() {
			if (gamestate == GAMESTATE_RUNNING) {
				int tempmv = 0;
				if (Pressed[P1LEFT]) {
					tempmv--;
				}
				if (Pressed[P1RIGHT]) {
					tempmv++;
				}
				int temptl = 0;
				if (Pressed[P1TILTLEFT]) {
					temptl--;
				}
				if (Pressed[P1TILTRIGHT]) {
					temptl++;
				}
				mainplayer.move(tempmv, temptl);
			}
			else if (gamestate == GAMESTATE_EDITOR) {
				if ((Pressed[EDITOR_MOVE_LEFT] || Pressed[EDITOR_MOVE_RIGHT] || Pressed[EDITOR_MOVE_UP] || Pressed[EDITOR_MOVE_DOWN])) {
					delay_count++;
					if (delay_count == 10) {
						if (Pressed[EDITOR_MOVE_LEFT]) {
							if (selectedBlock != -1) {
								if (editorMoveMode)
									blocks.get(selectedBlock).setCenterX(blocks.get(selectedBlock).getCenterX() - 10);
							}
						}
						if (Pressed[EDITOR_MOVE_RIGHT]) {
							if (selectedBlock != -1) {
								if (editorMoveMode)
									blocks.get(selectedBlock).setCenterX(blocks.get(selectedBlock).getCenterX() + 10);
							}
						}
						if (Pressed[EDITOR_MOVE_UP]) {
							if (selectedBlock != -1) {
								if (editorMoveMode)
									blocks.get(selectedBlock).setCenterY(blocks.get(selectedBlock).getCenterY() - 10);
							}
						}
						if (Pressed[EDITOR_MOVE_DOWN]) {
							if (selectedBlock != -1) {
								if (editorMoveMode)
									blocks.get(selectedBlock).setCenterY(blocks.get(selectedBlock).getCenterY() + 10);
							}
						}
					}
					else if (delay_count >= 12) {
						delay_count = 10 - 1;
					}
				}
			}
		}
	}


	public void paintComponent(Graphics g) {
		g.drawImage(backbuffer,0,0,null);
	}
	public class KeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			int key=e.getKeyCode();
			keyinput.keyPressed(key);
		}
		public void keyReleased(KeyEvent e) {
			int key=e.getKeyCode();
			keyinput.keyReleased(key);
		}
	}
	public class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (update()) //In order to display, update() must give us the OK.
				display((Graphics2D)backbuffer.getGraphics());
		}
	}
}
